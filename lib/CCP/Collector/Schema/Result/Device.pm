package CCP::Collector::Schema::Result::Device;

# ABSTRACT: DBIx::Class definition for "Device" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the C<Device>
table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.002000; # for unique_column

=method ID_TYPE

    # In definitions for other tables:
    column 'device_id' => {
        %{ CCP::Collector::Schema::Result::Device->ID_TYPE },
        is_foreign_key      => 1,
    };

Defines the particulars of the ID column for this table, so that other
tables using this column as a foreign key are defined correctly.

=cut

use constant ID_TYPE => {
    data_type           => 'int', # 0 .. 4_294_967_295
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};

table 'Device';

primary_column 'id' => {
    %{ ID_TYPE() },
    is_auto_increment   => 1,
};
column 'added' => {
    data_type           => 'datetime',
    is_numeric          => 0,
};
unique_column 'key' => {
    data_type           => 'char',
    size                => 32,
    is_numeric          => 0,
};

has_many 'submissions' => (
    'CCP::Collector::Schema::Result::Submission',
    'device_id',
);

1;
