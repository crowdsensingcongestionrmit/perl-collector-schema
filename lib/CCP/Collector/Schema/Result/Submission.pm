package CCP::Collector::Schema::Result::Submission;

# ABSTRACT: DBIx::Class definition for "Submission" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the
C<Submission> table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.001006; # for primary_column

use CCP::Collector::Schema::Result::Device ();

=method ID_TYPE

    # In definitions for other tables:
    column 'device_id' => {
        %{ CCP::Collector::Schema::Result::Submission->ID_TYPE },
        is_foreign_key      => 1,
    };

Defines the particulars of the ID column for this table, so that other
tables using this column as a foreign key are defined correctly.

=cut

use constant ID_TYPE => {
    data_type           => 'bigint', # 0 .. 18_446_744_073_709_551_615
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};

table 'Submission';

primary_column 'id' => {
    %{ ID_TYPE() },
    is_auto_increment   => 1,
};
column 'added' => {
    data_type           => 'datetime',
    is_numeric          => 0,
};
column 'device_id' => {
    %{ CCP::Collector::Schema::Result::Device->ID_TYPE },
    is_foreign_key      => 1,
};

belongs_to 'device' => (
    'CCP::Collector::Schema::Result::Device',
    'device_id',
);

has_many 'accelerometer_readings' => (
    'CCP::Collector::Schema::Result::AccelerometerReading',
    'submission_id',
);
has_many 'barometer_readings' => (
    'CCP::Collector::Schema::Result::BarometerReading',
    'submission_id',
);
has_many 'bluetooth_scans' => (
    'CCP::Collector::Schema::Result::BluetoothScan',
    'submission_id',
);
has_many 'gps_readings' => (
    'CCP::Collector::Schema::Result::GPSReading',
    'submission_id',
);
has_many 'magnetometer_readings' => (
    'CCP::Collector::Schema::Result::MagnetometerReading',
    'submission_id',
);
has_many 'thermometer_readings' => (
    'CCP::Collector::Schema::Result::ThermometerReading',
    'submission_id',
);
has_many 'wifi_scans' => (
    'CCP::Collector::Schema::Result::WiFiScan',
    'submission_id',
);

1;
