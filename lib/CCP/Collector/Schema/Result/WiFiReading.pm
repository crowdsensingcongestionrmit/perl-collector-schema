package CCP::Collector::Schema::Result::WiFiReading;

# ABSTRACT: DBIx::Class definition for "WiFiReading" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the
C<WiFiReading> table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.001006; # for primary_column

use CCP::Collector::Schema::Result::WiFiScan ();

table 'WiFiReading';

primary_column 'id' => {
    data_type           => 'bigint', # 0 .. 18_446_744_073_709_551_615
    extra               => { unsigned => 1 },
    is_numeric          => 1,
    is_auto_increment   => 1,
};
column 'wifi_scan_id' => {
    %{ CCP::Collector::Schema::Result::WiFiScan->ID_TYPE },
    is_foreign_key      => 1,
};
column 'time' => {
    data_type           => 'bigint',
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};
column 'mac_address' => {
    data_type           => 'char',
    size                => 12,
    is_numeric          => 0,
};
column 'network_name' => {
    data_type           => 'varchar',
    size                => 32,
    is_numeric          => 0,
    is_nullable         => 1,
};
column 'signal_level' => {
    data_type           => 'smallint', # -32_768 .. 32_767
    is_numeric          => 1,
};

belongs_to 'scan' => (
    'CCP::Collector::Schema::Result::WiFiScan',
    'wifi_scan_id',
);

1;
