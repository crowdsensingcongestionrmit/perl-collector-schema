package CCP::Collector::Schema::Result::BluetoothScan;

# ABSTRACT: DBIx::Class definition for "BluetoothScan" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the
C<BluetoothScan> table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.001006; # for primary_column

use CCP::Collector::Schema::Result::Submission ();

=method ID_TYPE

    # In definitions for other tables:
    column 'device_id' => {
        %{ CCP::Collector::Schema::Result::BluetoothScan->ID_TYPE },
        is_foreign_key      => 1,
    };

Defines the particulars of the ID column for this table, so that other
tables using this column as a foreign key are defined correctly.

=cut

use constant ID_TYPE => {
    data_type           => 'bigint', # 0 .. 18_446_744_073_709_551_615
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};

table 'BluetoothScan';

primary_column 'id' => {
    %{ ID_TYPE() },
    is_auto_increment   => 1,
};
column 'submission_id' => {
    %{ CCP::Collector::Schema::Result::Submission->ID_TYPE },
    is_foreign_key      => 1,
};
column 'start_time' => {
    data_type           => 'datetime',
    is_numeric          => 0,
};
column 'end_time' => {
    data_type           => 'datetime',
    is_numeric          => 0,
};

belongs_to 'submission' => (
    'CCP::Collector::Schema::Result::Submission',
    'submission_id',
);

has_many 'readings' => (
    'CCP::Collector::Schema::Result::BluetoothReading',
    'bluetooth_scan_id',
);

1;
