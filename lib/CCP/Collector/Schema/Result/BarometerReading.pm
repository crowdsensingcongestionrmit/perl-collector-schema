package CCP::Collector::Schema::Result::BarometerReading;

# ABSTRACT: DBIx::Class definition for "BarometerReading" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the
C<BarometerReading> table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.001006; # for primary_column

use CCP::Collector::Schema::Result::Submission ();

table 'BarometerReading';

primary_column 'id' => {
    data_type           => 'bigint', # 0 .. 18_446_744_073_709_551_615
    extra               => { unsigned => 1 },
    is_numeric          => 1,
    is_auto_increment   => 1,
};
column 'submission_id' => {
    %{ CCP::Collector::Schema::Result::Submission->ID_TYPE },
    is_foreign_key      => 1,
};
column 'time' => {
    data_type           => 'bigint',
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};
column 'pressure' => {
    data_type           => 'int', # 0 .. 4_294_967_295
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};

belongs_to 'submission' => (
    'CCP::Collector::Schema::Result::Submission',
    'submission_id',
);

1;
