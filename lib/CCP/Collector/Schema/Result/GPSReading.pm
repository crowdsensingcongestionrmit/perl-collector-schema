package CCP::Collector::Schema::Result::GPSReading;

# ABSTRACT: DBIx::Class definition for "GPSReading" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the C<GPSReading>
table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.001006; # for primary_column

use CCP::Collector::Schema::Result::Submission ();

table 'GPSReading';

primary_column 'id' => {
    data_type           => 'bigint', # 0 .. 18_446_744_073_709_551_615
    extra               => { unsigned => 1 },
    is_numeric          => 1,
    is_auto_increment   => 1,
};
column 'submission_id' => {
    %{ CCP::Collector::Schema::Result::Submission->ID_TYPE },
    is_foreign_key      => 1,
};
column 'time' => {
    data_type           => 'bigint',
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};
column 'latitude' => {
    data_type           => 'int', # -2_147_483_648 .. 2_147_483_647
    is_numeric          => 1,
};
column 'longitude' => {
    data_type           => 'int',
    is_numeric          => 1,
};
column 'accuracy' => {
    data_type           => 'mediumint', # 0 .. 16_777_215
    extra               => { unsigned => 1 },
    is_numeric          => 1,
};

belongs_to 'submission' => (
    'CCP::Collector::Schema::Result::Submission',
    'submission_id',
);

1;
