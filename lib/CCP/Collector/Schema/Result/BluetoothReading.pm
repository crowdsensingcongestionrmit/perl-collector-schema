package CCP::Collector::Schema::Result::BluetoothReading;

# ABSTRACT: DBIx::Class definition for "BluetoothReading" table

=head1 DESCRIPTION

This class defines a L<DBIx::Class::ResultSource> for the
C<BluetoothReading> table.

=cut

use strict;
use warnings;

use DBIx::Class::Candy 0.001006; # for primary_column

use CCP::Collector::Schema::Result::BluetoothScan ();

table 'BluetoothReading';

primary_column 'id' => {
    data_type           => 'bigint', # 0 .. 18_446_744_073_709_551_615
    extra               => { unsigned => 1 },
    is_numeric          => 1,
    is_auto_increment   => 1,
};
column 'bluetooth_scan_id' => {
    %{ CCP::Collector::Schema::Result::BluetoothScan->ID_TYPE },
    is_foreign_key      => 1,
};
column 'time' => {
    data_type           => 'datetime',
    is_numeric          => 0,
};
column 'mac_address' => {
    data_type           => 'char',
    size                => 12,
    is_numeric          => 0,
};
column 'device_name' => {
    data_type           => 'varchar',
    size                => 248,
    is_numeric          => 0,
    is_nullable         => 1,
};

belongs_to 'scan' => (
    'CCP::Collector::Schema::Result::BluetoothScan',
    'bluetooth_scan_id',
);

1;
