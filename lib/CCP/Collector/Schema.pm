package CCP::Collector::Schema;
our $VERSION = 1;

# ABSTRACT: CCP Collector database API

=head1 DESCRIPTION

This package uses L<DBIx::Class> to provide an object-oriented Perl API
over a pre-installed MySQL database.  It generates all of the SQL that
is necessary to communicate with the database, and it converts any
results from the database into Perl objects.

The main class of this package (and hence the primary target for this
documentation) is a L<DBIx::Class::Schema> subclass, with additional
support for connecting to the database using information stored in a
configuration file.

See L<DBIx::Class::Schema> for further methods available on this class.

=cut

use strict;
use warnings;

use base 'DBIx::Class::Schema';

use DBIx::Class 0.07999_01 (); # for load_namespaces
use Module::Find (); # for load_namespaces

use Config::Any 0.15 (); # for use_ext checks
use File::HomeDir ();
use Path::Class 'file';

__PACKAGE__->load_namespaces;

=method connect_using_config

    my $schema = $class->connect_using_config;

Connect to the database using details supplied in a configuration file,
and return a resultant C<CCP::Collector::Schema> instance.

The configuration file is expected to hold specific keys.  An example
in YAML format is shown here:

    ---
    db_host: mysql.crowdsensing.com.au
    db_name: collector
    db_user: collector_web
    db_pass: I0EVcD2h09BF8LXEVm7j9n8qUb36UwN4

The location (and format) of the file can be specified in the
C<CCP_COLLECTOR_CONFIG> environment variable.  If no such variable is
defined, C<$HOME/.ccp-collector.yaml> is used by default.

The specified database user must have C<SELECT> and C<INSERT>
privileges on the specified database.

=cut

sub connect_using_config {
    my $class = shift;

    # XXX: home directory might not be set in some circumstances.
    my $cfg_path = $ENV{'CCP_COLLECTOR_CONFIG'};
    $cfg_path = file(File::HomeDir->my_home, '.ccp-collector.yaml')
        if not defined $cfg_path;

    my $cfg = Config::Any->load_files({
        files   => [$cfg_path],
        use_ext => 1,
    });
    ($cfg) = values %{ $cfg->[0] }; # trim filename from data

    my $dsn = sprintf(
        'DBI:mysql:host=%s;database=%s',
        $cfg->{'db_host'},
        $cfg->{'db_name'},
    );
    return $class->connect({
        dsn         => $dsn,
        user        => $cfg->{'db_user'},
        password    => $cfg->{'db_pass'},
        quote_names => 1,
    });
}

=method devices

    my $devices_rs = $schema->devices;

Convenience method for C<< $schema->resultset('Device') >>.

=cut

sub devices { shift->resultset('Device') }

1;
