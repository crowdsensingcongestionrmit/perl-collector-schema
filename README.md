# NAME

CCP::Collector::Schema - CCP Collector database API

# VERSION

This module is part of distribution CCP-Collector-Schema v1.

This distribution's version numbering follows the conventions defined at [semver.org](http://semver.org/).

# DESCRIPTION

This package uses [DBIx::Class](https://metacpan.org/pod/DBIx::Class) to provide an object-oriented Perl API
over a pre-installed MySQL database.  It generates all of the SQL that
is necessary to communicate with the database, and it converts any
results from the database into Perl objects.

The main class of this package (and hence the primary target for this
documentation) is a [DBIx::Class::Schema](https://metacpan.org/pod/DBIx::Class::Schema) subclass, with additional
support for connecting to the database using information stored in a
configuration file.

See [DBIx::Class::Schema](https://metacpan.org/pod/DBIx::Class::Schema) for further methods available on this class.

# METHODS

## connect\_using\_config

    my $schema = $class->connect_using_config;

Connect to the database using details supplied in a configuration file,
and return a resultant `CCP::Collector::Schema` instance.

The configuration file is expected to hold specific keys.  An example
in YAML format is shown here:

    ---
    db_host: mysql.crowdsensing.com.au
    db_name: collector
    db_user: collector_web
    db_pass: I0EVcD2h09BF8LXEVm7j9n8qUb36UwN4

The location (and format) of the file can be specified in the
`CCP_COLLECTOR_CONFIG` environment variable.  If no such variable is
defined, `$HOME/.ccp-collector.yaml` is used by default.

The specified database user must have `SELECT` and `INSERT`
privileges on the specified database.

## devices

    my $devices_rs = $schema->devices;

Convenience method for `$schema->resultset('Device')`.

# SUPPORT

## Bugs / Feature Requests

Please report any bugs or feature requests by email to `bug-ccp-collector-schema at rt.cpan.org`, or through
the web interface at [http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CCP-Collector-Schema](http://rt.cpan.org/NoAuth/ReportBug.html?Queue=CCP-Collector-Schema). You will be automatically notified of any
progress on the request by the system.

## Source Code

The source code for this distribution is available online in a [Git](http://git-scm.com/) repository.  Please feel welcome to contribute patches.

[https://github.com/xxx/xxx](https://github.com/xxx/xxx)

    git clone git://github.com/xxx/xxx

# AUTHOR

Alex Peters <s3105178@student.rmit.edu.au>

# COPYRIGHT AND LICENSE

This software is copyright (c) 2014 by Congestion Crowdsensing Project.  No
license is granted to other entities.

The full text of the license can be found in the
`LICENSE` file included with this distribution.
